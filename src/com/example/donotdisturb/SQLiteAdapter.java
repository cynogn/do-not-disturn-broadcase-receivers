package com.example.donotdisturb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteAdapter {

	private static final String MYDATABASE_NAME = "DONOTDISTURB";
	private static final String MYDATABASE_TABLE = "CALLLOG";
	private static final int MYDATABASE_VERSION = 1;
	public static final String KEY_ID = "_id";
	public static final String KEY_COLUMN1 = "phoneNumber";
	public static final String KEY_COLUMN2 = "startTime";
	public static final String KEY_COLUMN3 = "Endtime";
	public static final String KEY_COLUMN4 = "Duration";
	public static final String KEY_COLUMN5 = "status";

	private static final String SCRIPT_CREATE_DATABASE = "create table "
			+ MYDATABASE_TABLE + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_COLUMN1 + " text,"
			+ KEY_COLUMN2 + " text unique ," + KEY_COLUMN3 + " text unique ,"
			+ KEY_COLUMN4 + " text, " + KEY_COLUMN5 + " text );";

	private SQLiteHelper sqLiteHelper;
	private SQLiteDatabase sqLiteDatabase;

	private Context context;

	public SQLiteAdapter(Context c) {
		context = c;
	}

	public SQLiteAdapter openToRead() throws android.database.SQLException {
		sqLiteHelper = new SQLiteHelper(context, MYDATABASE_NAME, null,
				MYDATABASE_VERSION);
		sqLiteDatabase = sqLiteHelper.getReadableDatabase();
		return this;
	}

	public SQLiteAdapter openToWrite() throws android.database.SQLException {
		sqLiteHelper = new SQLiteHelper(context, MYDATABASE_NAME, null,
				MYDATABASE_VERSION);
		sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		sqLiteHelper.close();
	}

	public long insert(String value1, String value2, String value3,
			String value4, String value5) {
		ContentValues values = new ContentValues();
		if (value1 != null)
			values.put(KEY_COLUMN1, value1.toString());
		else
			values.put(KEY_COLUMN1, "PHONE NUMBER IS NULL");
		values.put(KEY_COLUMN2, value2.toString());
		values.put(KEY_COLUMN3, value3.toString());
		values.put(KEY_COLUMN4, value4.toString());
		values.put(KEY_COLUMN5, value5.toString());

		return sqLiteDatabase.insert(MYDATABASE_TABLE, null, values);
	}

	public int deleteAll() {
		return sqLiteDatabase.delete(MYDATABASE_TABLE, null, null);
	}

	public Cursor queueAll() {

		Cursor cursor = sqLiteDatabase.query(MYDATABASE_TABLE, null, null,
				null, null, null, "_id desc");

		return cursor;
	}

	public class SQLiteHelper extends SQLiteOpenHelper {

		public SQLiteHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(SCRIPT_CREATE_DATABASE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

	}

}