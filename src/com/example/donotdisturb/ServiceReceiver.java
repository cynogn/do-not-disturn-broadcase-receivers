package com.example.donotdisturb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Service Receiver listens for a broadcast receiver
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 */
public class ServiceReceiver extends BroadcastReceiver {
	private SQLiteAdapter mDatabase;
	private String mIncomingNumber;
	int i = 0;

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		if (bundle.getString("incoming_number") != null) {
			mIncomingNumber = bundle.getString("incoming_number");
		}
		MyPhoneStateListener phoneListener = new MyPhoneStateListener();
		mDatabase = new SQLiteAdapter(context);
		mDatabase.openToWrite();
		TelephonyManager telephony = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

	}

	/**
	 * Phone lsiter calss lsiterns to phone state change
	 * 
	 * @author Gautam Balasubramanian
	 * 
	 */
	public class MyPhoneStateListener extends PhoneStateListener {
		String attendTime = null;
		String endTime = null;
		String newStart = null;
		String status = null;

		/**
		 * Finds difference between two time intervals (Value1 - Value2)
		 * 
		 * @param Time1
		 * @param Time2
		 * @return
		 * @throws ParseException
		 */
		long subtractTime(String value1, String Value2) throws ParseException {
			java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm:ss");
			java.util.Date date1 = df.parse(value1);
			java.util.Date date2 = df.parse(Value2);
			long diff = date2.getTime() - date1.getTime();
			return diff;

		}

		/**
		 * Onstate change this will be called (non-Javadoc)
		 * 
		 * @see android.telephony.PhoneStateListener#onCallStateChanged(int,
		 *      java.lang.String)
		 */
		public void onCallStateChanged(int state, String incomingNumber) {

			Calendar c = Calendar.getInstance();
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				SimpleDateFormat df1 = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				endTime = df1.format(c.getTime());
				Log.v("ENDED", "Call ended");
				if (newStart != null && endTime != null && status == "attended") {
					Log.d("DoNotDisturb", "Attended @" + newStart);
					Log.d("DoNotDisturb", "Ended @" + endTime);
					try {
						long duration = subtractTime(
								newStart.toString().split(" ")[1], endTime
										.toString().split(" ")[1]);
						Log.v("Duration", duration / 1000 + " Sec");
						Log.v("phonenumber", mIncomingNumber + " is the numebr");

						if (mIncomingNumber != null) {
							mDatabase.insert(mIncomingNumber, newStart,
									endTime, duration / 1000 + " Sec",
									"Received");
							status = "attended";
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else {
					if (status == null) {
						if (i == 0) {
							i++;
							Log.v("MISSED", "MISSED");
							Cursor corsor = mDatabase.queueAll();
							if (corsor.getCount() > 0)
								corsor.moveToFirst();
							if (mIncomingNumber != null)
								Log.v("CURSOR", corsor.getString(1));
							Log.v("PHONENUM", mIncomingNumber);
							if ((corsor.getString(1)
									.equals(corsor.getString(1)))) {
							} else {
								mDatabase.insert(mIncomingNumber, endTime
										.toString().split(" ")[1], endTime
										.toString().split(" ")[1], "0 Sec",
										"Missed");
							}
						}
					}

				}

				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				attendTime = df.format(c.getTime());
				if (attendTime != null) {
					newStart = attendTime;
					status = "attended";
				}
				break;
			default: {
				Log.d("DoNotDisturb", "Call From @" + incomingNumber);
				int i = 0;
				status = null;

			}
			}
		}
	}
}
